# README #

Face recognition for additional tool for biometric identification within Cashpoint project

## 1. Setup ##

This setup is made for MacOSX 10.12.6 environment  
Requirements:

* python 3
* docker
* boost-python
* XQuartz
* dlib

## 2. Contribution guidelines ##

* Writing tests
* Code review
* Other guidelines

## 3. Contact ##

* Ardya Dipta Nandaviri (ardyadipta@gmail.com)
* Ghifari Dwiki Ramadhan (ghifari.dwiki@amartha.com)

## 4. Working Notes ##

### 4.1. System setup (If you wish to simply use docker instead, go to 4.2) ###
* Install brew if you have not

```
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"     
brew update    
```

* Give access to brew  
`sudo chown -R "$USER":admin /usr/local`

* [Install dlib refer to this link](https://www.pyimagesearch.com/2017/03/27/how-to-install-dlib/) 

* [Install XQuartz](https://www.xquartz.org/) 

### 4.2. Docker Setup ###
* We are using opensource library openface for this project, the environment has been setup in this docker

```
docker pull bamos/openface
docker run -p 9000:9000 -p 8000:8000 -t -i bamos/openface /bin/bash
cd /root/openface
```

### 4.3. Training steps ###
* All of this steps are done in the docker environment /root/openface/
* Put training images in this folder format: /root/openface/training_images/\<personID\>/\<pictures\>
* Do pose detection and alignment  

`./util/align-dlib.py ./training_images/ align outerEyesAndNose ./aligned_images/ --size 96`

* generate representation from align images  

`./batch-represent/main.lua -outDir ./generated-embeddings/ -data ./aligned_images/`

* train face detection model  

`./demos/classifier.py train ./generated-embeddings/`
 
* Recognize face  

`./demos/classifier.py infer ./generated-embeddings/classifier.pkl your_test_image.jpg`

### 4.4. Retraining ###
cd ~/openface

* Clear up previous training environment  

```
rm /root/openface/aligned_images/cache.t7
rm -r training_images
rm -r testing_images
rm -r aligned_images
rm -r generated-embeddings/*
```

* Copy or move the new training images and testing images from your host environment 

```
cp -r /host/Users/<path to training images folder> .
cp -r /host/Users/<path to testing images folder> .
```

* Same step to retrain from previous steps

```
./util/align-dlib.py ./training_images/ align outerEyesAndNose ./aligned_images/ --size 96  
./batch-represent/main.lua -outDir ./generated-embeddings/ -data ./aligned_images/  
./demos/classifier.py train ./generated-embeddings/  
./demos/classifier.py infer ./generated-embeddings/classifier.pkl testing_images/<test image>  
```